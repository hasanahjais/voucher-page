import { Component, Inject, OnInit } from '@angular/core';
import { AbstractControl, FormBuilder, FormControl, FormGroup, Validators } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { category, listOfStatus } from 'src/app/shared/data-collection';
import { ServerService } from '../shared/server.service';

@Component({
  selector: 'app-edit',
  templateUrl: './edit.component.html',
  styleUrls: ['./edit.component.scss']
})
export class EditComponent implements OnInit {
  public editForm: FormGroup;

  public listOfControls = [];
  public allStatus = listOfStatus;
  public category = category;

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private fb: FormBuilder, private server: ServerService, private matDiaRef: MatDialogRef<any>) { }

  ngOnInit() {
    this.editForm = this.fb.group({});

    Object.keys(this.data).forEach(prop => {
      if (prop !== 'voucherid' && prop !== 'voucher_code' &&
        prop !== 'product' && prop !== 'voucher_type' && prop !== 'description'
      ) {
        let value = null;
        let propName = prop;
        const desc = this.data['description'];

        if (propName !== 'discount') {
          value = this.data[propName];
        } else {
          value = Number(this.data[propName]);
        }

        const formCtrl = new FormControl(value, []);
        this.editForm.addControl(propName, formCtrl);
        this.listOfControls.push(propName);
      }

    });
  }

  public editVoucher() {
    const newObj = { ...this.data, ...this.editForm.value };

    if (newObj.hasOwnProperty('discount')) {
      newObj['discount'] = String(newObj['discount']);
    }

    this.server.updateVoucher(newObj).subscribe(resp => {
      if (resp) {
        this.server.setTasks('update-voucher', newObj);
        this.matDiaRef.close();
      }
    });
  }

}
