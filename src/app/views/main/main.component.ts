import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

@Component({
  selector: 'app-main',
  templateUrl: './main.component.html',
  styleUrls: ['./main.component.scss']
})
export class MainComponent implements OnInit {

  constructor(private router: Router) { }

  ngOnInit() {
    this.showTemplate('Create');
  }

  // show sections of the voucher page
  public showTemplate(page) {
    const showPage = typeof page === 'object' ? page.tab.textLabel : page;

    switch (showPage) {
      case 'Create':
        this.router.navigate(['voucher/create']);
        break;

      case 'View':
        this.router.navigate(['voucher/view']);
        break;

      default:
        break;
    }
  }
}
