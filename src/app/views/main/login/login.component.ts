import { Component, OnInit } from '@angular/core';
import { FormGroup, FormBuilder, Validators } from '@angular/forms';
import { ServerService } from 'src/app/shared/server.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  public loginForm: FormGroup;

  constructor(private fb: FormBuilder, private server: ServerService, private router: Router) { }

  ngOnInit() {
    this.init();
  }

  // init form
  private init() {
    this.loginForm = this.fb.group({
      username: ['', [Validators.required]],
      password: ['', [Validators.required]]
    });
  }

  public submit() {
    const uName = this.loginForm.get('username').value;
    const password = this.loginForm.get('password').value;

    const user = {
      user_id: uName,
      password
    };

    this.server.login(user)
      .then(res => {
        if (res) {
          this.router.navigate(['voucher/create']);
        }

      })
      .catch(error => console.log(error));
  }
}
