import { Component, OnInit, AfterViewInit } from '@angular/core';
import { MatDialog } from '@angular/material/dialog';
import { EditComponent } from 'src/app/edit/edit.component';
import { ServerService } from 'src/app/shared/server.service';
import { listOfVoucherTypes } from 'src/app/shared/data-collection';
import { RemoveComponent } from 'src/app/shared/remove/remove.component';

@Component({
  selector: 'app-view',
  templateUrl: './view.component.html',
  styleUrls: ['./view.component.scss']
})
export class ViewComponent implements OnInit, AfterViewInit {
  public showTable = false;
  public listOfAllVouchers = [];
  public filteredVouchers = [];

  public voucherTypes = listOfVoucherTypes;

  public displayedColumns = ['voucher_type', 'voucher_code', 'discount', 'date_expired', 'action'];

  public currentSelectedVoucher = 'all';

  constructor(private server: ServerService, private dialog: MatDialog) { }

  ngOnInit() {

  }

  ngAfterViewInit() {
    this.init();
  }

  private init(): void {
    this.getAllVoucher();

    this.server.getTasks().subscribe(task => {
      if (task) {
        if (task.event === 'remove-voucher') {

          for (let v = 0; v < this.listOfAllVouchers.length; v++) {
            if (this.listOfAllVouchers[v].voucherid === task.data.voucherid) {
              this.listOfAllVouchers.splice(v, 1);
              break;
            }

          }

        }

        if (task.event === 'update-voucher') {
          for (let v = 0; v < this.listOfAllVouchers.length; v++) {
            if (this.listOfAllVouchers[v].voucherid === task.data.voucherid) {
              this.listOfAllVouchers[v] = task.data;
              break;
            }

          }
        }

        this.filteredVouchers = this.listOfAllVouchers.filter(voucher => {
          if (this.currentSelectedVoucher !== 'all') {
            return voucher.voucher_type === this.currentSelectedVoucher;
          } else {
            return true;
          }
        });
      }
    });
  }

  // get all the voucher from server
  private getAllVoucher() {
    this.server.getAllVouchers().subscribe((vouchers) => {
      const results = (vouchers as any).results;

      for (let v = 0; v < results.length; v++) {
        this.listOfAllVouchers.push(results[v]);
      }

      this.filteredVouchers = this.listOfAllVouchers;

      setTimeout(() => {
        this.showTable = true;
      }, 1000);

    }, error => {
      console.log(error);
    });
  }

  public filterVoucher(type) {
    const value = type.value;
    this.currentSelectedVoucher = value;
    if (value === 'all') {
      this.filteredVouchers = this.listOfAllVouchers;
    }

    if (value === 'iot') {
      this.filteredVouchers = this.listOfAllVouchers.filter(voucher => voucher.voucher_type === 'iot');
    }

    if (value === 'dscover') {
      this.filteredVouchers = this.listOfAllVouchers.filter(voucher => {
        if (voucher.voucher_type === 'dscover') {
          return true;
        }

        return false;
      });
    }
  }

  public openEditVoucher(voucher) {
    this.dialog.open(EditComponent, {
      height: '400px',
      width: '600px',
      data: voucher
    });
  }

  openRemoveVoucher(voucher) {
    this.dialog.open(RemoveComponent, {
      height: '400px',
      width: '600px',
      data: voucher
    });
  }

}
