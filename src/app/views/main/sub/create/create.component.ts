import { Component, OnInit } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';
import { MatDialog } from '@angular/material/dialog';

// Components
import { VoucherComponent } from '../../../../shared/dialog/voucher/voucher.component';

// the `default as` syntax.
import * as moment from 'moment';
import { ServerService } from 'src/app/shared/server.service';
import { category, listOfStatus, listOfVoucherTypes, subscription } from 'src/app/shared/data-collection';

@Component({
  selector: 'app-create',
  templateUrl: './create.component.html',
  styleUrls: ['./create.component.scss']
})
export class CreateComponent implements OnInit {
  // voucher form
  public voucherForm: FormGroup;

  public listOfVoucherTypes = listOfVoucherTypes;
  public listOfStatus = listOfStatus;
  public category = category;
  public subscription = subscription;

  constructor(private server: ServerService, private fb: FormBuilder, private dialog: MatDialog) {

    this.voucherForm = this.fb.group({
      'type': ['iot', [Validators.required]],
      'name': [null, []],
      'dateExpiration': [moment(), [Validators.required]],
      'dateCreated': [moment(), [Validators.required]],
      'count': [1, [Validators.required]],
      'generateNum': [1, [Validators.required]],
      'discount': [0, [Validators.required]],
      'status': ['draft', [Validators.required]],

      iot: this.fb.group({
        'training': ['false', []],
        'extendYear': [0, [Validators.required]],
        'category': ['promotion', []]
      }),

      dscover: this.fb.group({
        subscription: 'monthly'
      })
    });
  }

  ngOnInit() { }

  public returnStatus(operation): string[] {
    if (operation === 'create') {
      return this.listOfStatus.filter(status => {
        if (!(status === 'ended' || status === 'ongoing')) {
          return status;
        }
      });
    }

    return this.listOfStatus;
  }

  get returnCurrentVoucherType(): string {
    return this.voucherForm.get('type').value;
  }

  // submit voucher details
  public async submitVoucher() {
    // get voucher details
    const type = this.voucherForm.value.type;
    const name = this.voucherForm.value.name;
    const status = this.voucherForm.value.status;
    const dateCreate = new Date(this.voucherForm.value.dateCreated).toISOString();
    const dateExp = new Date(this.voucherForm.value.dateExpiration).toISOString();
    const discount = this.voucherForm.value.discount;
    const count = this.voucherForm.value.count;
    const howManyVouchers = this.voucherForm.value.generateNum;
    let description = {};

    const voucherObj = {
      voucher_code: name,
      voucher_type: type,
      date_created: dateCreate,
      date_expired: dateExp,
      discount: discount.toString(),
      count: Number(count),
      description: {},
      status,
      product: ''
    };

    if (type === 'iot') {
      const category = this.voucherForm.value.iot.category;
      const descript = {
        extendYear: this.voucherForm.value.iot.extendYear.toString(),
        isTraining: this.voucherForm.value.iot.training.toString()
      }
      voucherObj.description = JSON.stringify(descript);

      // check if discount value is more than 100
      // if yes stop
      if (discount > 100) {
        alert('Discount cannot be more than 100');

        return;
      }

      // voucher object
      voucherObj['category'] = category;
      voucherObj.product = 'platform';
    }

    if (type === 'dscover') {
      const desc = {
        subscription: this.voucherForm.value.dscover.subscription
      };

      voucherObj.description = JSON.stringify(desc);
      voucherObj.product = 'dscover';
    }

    this.processVoucher(type, voucherObj, howManyVouchers);
  }

  public async processVoucher(type, voucherObject, howMany) {
    const arrayOfVouchers = [];
    let generateCount = howMany;

    while (generateCount > 0) {
      let finalVoucherObj = {};

      let voucher;
      try {
        voucher = await this.server.submitNewVoucher(voucherObject) as any;
      } catch (e) {
        console.log(e);
        throw e;
      }

      // format it - with previous voucher obj and server voucher obj
      const descObj = JSON.parse(voucher.description);

      // // for final display to user
      if (type === 'iot') {
        finalVoucherObj = {
          type: voucherObject.voucher_type,
          code: voucherObject.voucher_code || voucher.voucher_code,
          discount: voucher.discount,
          expires: voucher.date_expired,
          isTraining: descObj.isTraining,
          extendYears: descObj.extendYear,
          status: voucherObject.status,
          count: voucherObject.count
        };
      }

      if (type === 'dscover') {
        finalVoucherObj = {
          type: voucherObject.voucher_type,
          code: voucherObject.voucher_code || voucher.voucher_code,
          discount: voucher.discount,
          expires: voucher.date_expired,
          status: voucherObject.status,
          count: voucherObject.count,
          subscription: JSON.parse(voucherObject.description).subscription
        };
      }

      arrayOfVouchers.push(finalVoucherObj);

      generateCount -= 1;

      this.dialog.open(VoucherComponent, {
        data: arrayOfVouchers
      });
    }

  }


}
