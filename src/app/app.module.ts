// Modules
import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

// Material Angular
import { MatTabsModule } from '@angular/material/tabs';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { MatCardModule } from '@angular/material/card';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatSelectModule } from '@angular/material/select';
import { MatDatepickerModule } from '@angular/material/datepicker';
import { MatMomentDateModule, } from '@angular/material-moment-adapter';
import { MatButtonModule } from '@angular/material/button';
import { MatTableModule } from '@angular/material/table';
import { MatPaginatorModule } from '@angular/material/paginator';
import { MatDialogModule } from '@angular/material/dialog';
import { MatRadioModule } from '@angular/material/radio';


// Others
import { NgxQRCodeModule } from 'ngx-qrcode2';

// Components
import { AppComponent } from './app.component';
import { MainComponent } from './views/main/main.component';
import { CreateComponent } from './views/main/sub/create/create.component';
import { ViewComponent } from './views/main/sub/view/view.component';
import { ServerService } from './shared/server.service';
import { VoucherComponent } from './shared/dialog/voucher/voucher.component';
import { LoginComponent } from './views/main/login/login.component';
import { AuthGardService } from './shared/auth-gard.service';
import { EditComponent } from './edit/edit.component';
import { RemoveComponent } from './shared/remove/remove.component';

const routes: Routes = [
  {
    path: 'login', component: LoginComponent
  },
  {
    path: 'voucher', component: MainComponent, canActivate: [AuthGardService], children: [
      { path: 'create', component: CreateComponent },
      { path: 'view', component: ViewComponent },

    ]
  },
  { path: '**', redirectTo: 'login' }
];

@NgModule({
  declarations: [
    AppComponent,
    MainComponent,
    CreateComponent,
    ViewComponent,
    VoucherComponent,
    LoginComponent,
    EditComponent,
    RemoveComponent
  ],
  imports: [
    BrowserModule,
    RouterModule.forRoot(routes),
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    BrowserAnimationsModule,
    NgxQRCodeModule,
    MatCardModule,
    MatTabsModule,
    MatFormFieldModule,
    MatInputModule,
    MatSelectModule,
    MatDatepickerModule,
    MatMomentDateModule,
    MatButtonModule,
    MatDialogModule,
    MatTableModule,
    MatPaginatorModule,
    MatRadioModule
  ],
  providers: [ServerService],
  entryComponents: [VoucherComponent, EditComponent, RemoveComponent],
  bootstrap: [AppComponent]
})
export class AppModule { }
