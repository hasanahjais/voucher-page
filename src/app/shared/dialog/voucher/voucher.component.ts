import { Component, OnInit, Inject } from '@angular/core';
import { MAT_DIALOG_DATA } from '@angular/material/dialog';
// import moment = require('moment');

@Component({
  selector: 'app-voucher',
  templateUrl: './voucher.component.html',
  styleUrls: ['./voucher.component.scss']
})
export class VoucherComponent implements OnInit {
  public listOfAllVouchers = [];

  constructor(@Inject(MAT_DIALOG_DATA) public data: any) {
    const voucherArray: [] = data;

    voucherArray.forEach(voucher => {
      this.listOfAllVouchers.push(voucher);
    });
  }

  ngOnInit() { }
}
