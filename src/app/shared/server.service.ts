import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { UtilityService } from './utility.service';
import { BehaviorSubject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class ServerService {
  private url = environment.paymentUrl;
  private loginUrl = environment.apiUrlWOVersion + 'favoriotadmin/authenticate';

  private tasks = new BehaviorSubject(null);

  constructor(private httpClient: HttpClient, private utils: UtilityService) { }

  // generates simple header
  private generateHeader() {

    return new HttpHeaders({ 'content-type': 'application/json' });
  }

  // submits voucher to server
  public submitNewVoucher(voucherObj) {
    const header = this.generateHeader();

    return this.httpClient.post(this.url + 'voucher/create', voucherObj, { headers: header })
      .toPromise()
      .then(this.extract);
  }

  // get voucher from server
  public getAllVouchers() {
    const header = this.generateHeader();

    return this.httpClient.get(this.url + 'voucher/list', { headers: header });
  }

  public updateVoucher(voucher) {
    const header = this.generateHeader();

    return this.httpClient.post(this.url + 'voucher/edit', voucher, { headers: header });
  }

  public removeVoucher(voucherId) {
    const header = this.generateHeader();

    return this.httpClient.delete(this.url + 'voucher?voucherid=' + voucherId, { headers: header });
  }

  // logs in user
  public login(user: any): Promise<any> {
    return this.httpClient.post(this.loginUrl, JSON.stringify(user), { headers: this.generateHeader() })
      .toPromise()
      .then(res => this.extract(res))
      .catch(error => {
        console.log(error);
        return error;
      });
  }

  private extract(res) {
    if ((res.code === 200 || res.statusCode === 200) && 'results' in res) {
      return res.results;
    }

    if ((res.code === 200 || res.statusCode === 200) && 'message' in res) {
      this.utils.saveData(res.message, 'user');

      return res.message;
    }

    if (res.statusCode === 4011) {
      throw new Error(res.error.message);
    }
  }

  public setTasks(event: string, data: any) {
    const obj = {
      event,
      data
    };
    this.tasks.next(obj);
  }

  public getTasks() {
    return this.tasks.asObservable();
  }
}


