import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class UtilityService {

  constructor() { }

  // save ls
  public saveData(data: any, nameOfData: string) {
    localStorage.setItem(nameOfData, JSON.stringify(data));
  }

  // get ls
  public getData(nameOfData: string) {
    return JSON.parse(localStorage.getItem(nameOfData));
  }
}
