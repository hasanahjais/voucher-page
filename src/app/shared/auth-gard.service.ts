import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router/src/utils/preactivation';
import { Router, ActivatedRouteSnapshot } from '@angular/router';
import { UtilityService } from './utility.service';

@Injectable({
  providedIn: 'root'
})
export class AuthGardService implements CanActivate {
  path: ActivatedRouteSnapshot[];
  route: ActivatedRouteSnapshot;
  constructor(public router: Router, private utils: UtilityService) { }

  canActivate(): boolean {
    const user = this.utils.getData('user');

    if (user) {
      return true;
    } else {
      this.router.navigate(['login']);
      return false;
    }
  }
}
