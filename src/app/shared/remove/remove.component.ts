import { Component, Inject, OnInit } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { ServerService } from '../server.service';

@Component({
  selector: 'app-remove',
  templateUrl: './remove.component.html',
  styleUrls: ['./remove.component.scss']
})
export class RemoveComponent implements OnInit {

  constructor(@Inject(MAT_DIALOG_DATA) public data: any, private server: ServerService, private matRef: MatDialogRef<any>) { }

  ngOnInit() { }

  public confirm() {
    this.server.removeVoucher(this.data.voucherid).subscribe(response => {
      this.server.setTasks('remove-voucher', { voucherid: this.data.voucherid });
      this.matRef.close();
    });

  }
}
