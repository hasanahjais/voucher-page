// collection of all available voucher platform
export const listOfVoucherTypes = [
    { name: 'IOT-Platform', value: 'iot' },
    { name: 'D\'scover', value: 'dscover' },
];

export const listOfStatus = [
    'draft',
    'live',
    'ended',
    'ongoing'
];

export const category = [
    'promotion',
    'individual'
];

export const subscription = [
    'monthly',
    'yearly'
];
